package com.example.basicmathsquiz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.basicmathsquiz.BaseActivity;
import com.example.basicmathsquiz.model.AnswerModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Answer extends MyDatabase{
    public static final String TABLE_NAME="Answer";
    public static final String ID="id";
    public static final String OPERATION="operation";
    public static final String DIGIT="digit";
    public static final String ATTEMP_QUESTION="attemp_question";
    public static final String RIGHT="right_ansewr";
    public static final String WRONG="rong_answer";
    public static final String DATE="date";

    public Answer(Context context) {
        super(context);
    }
    public long insertAnswer(String operation,String digit,int attemp_question,int right_answer,int rong_answer,String date){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(OPERATION,operation);
        cv.put(DIGIT,digit);
        cv.put(ATTEMP_QUESTION,attemp_question);
        cv.put(RIGHT,right_answer);
        cv.put(WRONG,rong_answer);
        cv.put(DATE,date);
        long lastInsertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertedId;
    }
    public ArrayList<AnswerModel> getAnswerList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<AnswerModel> list= new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for(int i = 0 ; i < cursor.getCount(); i++){
            AnswerModel answerModel = new AnswerModel();
            answerModel.setId(cursor.getInt(cursor.getColumnIndex(ID)));
            answerModel.setOperation(cursor.getString(cursor.getColumnIndex(OPERATION)));
            answerModel.setDigit(cursor.getString(cursor.getColumnIndex(DIGIT)));
            answerModel.setAttemp_question(cursor.getInt(cursor.getColumnIndex(ATTEMP_QUESTION)));
            answerModel.setRight_ansewr(cursor.getInt(cursor.getColumnIndex(RIGHT)));
            answerModel.setRong_answer(cursor.getInt(cursor.getColumnIndex(WRONG)));
            answerModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
            list.add(answerModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public int deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "1", null);
    }
}
