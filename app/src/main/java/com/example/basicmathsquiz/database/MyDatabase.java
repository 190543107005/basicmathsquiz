package com.example.basicmathsquiz.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {
    public static final String DATABASE_NAME = "BasicMathsQuiz.db";
    public MyDatabase(Context context) {
        super(context, DATABASE_NAME,null, 1);
    }
}
