package com.example.basicmathsquiz;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basicmathsquiz.adapter.AnswerListAdapter;
import com.example.basicmathsquiz.database.Answer;
import com.example.basicmathsquiz.model.AnswerModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswerListActivity extends BaseActivity {
    ArrayList<AnswerModel> answerLst = new ArrayList<>();
    AnswerListAdapter adapter;
    @BindView(R.id.rcvAnswerList)
    RecyclerView rcvAnswerList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_list);
        ButterKnife.bind(this);
        setupActionBar("AnswerSheet", true);
        setAdapter();
    }
    void setAdapter(){
        rcvAnswerList.setLayoutManager(new GridLayoutManager(this,1));
        answerLst.addAll(new Answer(this).getAnswerList());
        adapter = new AnswerListAdapter(answerLst,this);
        rcvAnswerList.setAdapter(adapter);
    }

}
