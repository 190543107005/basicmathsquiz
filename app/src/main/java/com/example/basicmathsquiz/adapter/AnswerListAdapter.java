package com.example.basicmathsquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basicmathsquiz.R;
import com.example.basicmathsquiz.model.AnswerModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnswerListAdapter extends RecyclerView.Adapter<AnswerListAdapter.AnswerHolder> {
    ArrayList<AnswerModel> answerList;
    Context context;

    public AnswerListAdapter(ArrayList<AnswerModel> answerList, Context context) {
        this.answerList = answerList;
        this.context = context;
    }

    @NonNull
    @Override
    public AnswerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AnswerHolder(LayoutInflater.from(context).inflate(R.layout.view_answer, null));
    }

    @Override
    public void onBindViewHolder(@NonNull AnswerHolder holder, int position) {
        holder.tvDate.setText("DATE  : " + answerList.get(position).getDate());
        holder.tvData.setText("Operation  :  " + answerList.get(position).getOperation() + " " + answerList.get(position).getDigit());
        holder.tvAttempt.setText("Attempt : "+answerList.get(position).getAttemp_question());
        holder.tvright.setText("correct : " + answerList.get(position).getRight_ansewr());
        holder.tvwrong.setText("incorrect : " + answerList.get(position).getRong_answer());
    }

    @Override
    public int getItemCount() {
        return answerList.size();
    }

    class AnswerHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvData)
        TextView tvData;
        @BindView(R.id.tvright)
        TextView tvright;
        @BindView(R.id.tvwrong)
        TextView tvwrong;
        @BindView(R.id.tvAttempt)
        TextView tvAttempt;

        public AnswerHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
