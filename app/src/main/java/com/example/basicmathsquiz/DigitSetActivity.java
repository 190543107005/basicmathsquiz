package com.example.basicmathsquiz;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DigitSetActivity extends BaseActivity {
    @BindView(R.id.tvImage1)
    ImageView tvImage1;
    @BindView(R.id.llBackground1)
    LinearLayout llBackground1;
    @BindView(R.id.tvText1)
    TextView tvText1;
    @BindView(R.id.tvExample1)
    TextView tvExample1;
    @BindView(R.id.cv1)
    CardView cv1;
    @BindView(R.id.tvImage2)
    ImageView tvImage2;
    @BindView(R.id.llBackground2)
    LinearLayout llBackground2;
    @BindView(R.id.tvText2)
    TextView tvText2;
    @BindView(R.id.tvExample2)
    TextView tvExample2;
    @BindView(R.id.cv2)
    CardView cv2;
    @BindView(R.id.tvImage3)
    ImageView tvImage3;
    @BindView(R.id.llBackground3)
    LinearLayout llBackground3;
    @BindView(R.id.tvText3)
    TextView tvText3;
    @BindView(R.id.tvExample3)
    TextView tvExample3;
    @BindView(R.id.cv3)
    CardView cv3;
    @BindView(R.id.tvImage4)
    ImageView tvImage4;
    @BindView(R.id.llBackground4)
    LinearLayout llBackground4;
    @BindView(R.id.tvText4)
    TextView tvText4;
    @BindView(R.id.tvExample4)
    TextView tvExample4;
    @BindView(R.id.cv4)
    CardView cv4;
    String s1= "1 digit with 1",s2="2 digit with 1",s3="2 digit with 2",s4="3 digit with 2";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.digits_set);
        ButterKnife.bind(this);
        setupActionBar("Select Digit", true);
        setData();
    }
    void setData(){
        String str = getIntent().getStringExtra("digit_key");
        if (str.equals("substraction")){
            tvImage2.setImageResource(R.drawable.substraction);
            tvImage3.setImageResource(R.drawable.substraction);
            tvImage4.setImageResource(R.drawable.substraction);
            setDigit("Subtract"+s1,"Subtract"+s2,"Subtract"+s3,
                    "Subtract"+s4,"Ex:- 9 - 5 = 4","Ex:- 12 - 9 = 3","Ex:- 13 - 11 = 2","Ex:- 100 - 90 = 10");
        }else if(str.equals("multiply")){
            tvImage1.setImageResource(R.drawable.multiplication);
            tvImage2.setImageResource(R.drawable.multiplication);
            tvImage3.setImageResource(R.drawable.multiplication);
            tvImage4.setImageResource(R.drawable.multiplication);
            setDigit("Multiply"+s1,"Multiply"+s2,"Multiply"+s3,
                    "Multiply"+s4,"Ex:- 9 * 5 = 45","Ex:- 20  * 9 = 180","Ex:- 13 * 11 = 143","Ex:- 100 * 10 = 1000");
        }else if(str.equals("division")){
            tvImage1.setImageResource(R.drawable.devide1);
            tvImage2.setImageResource(R.drawable.devide1);
            tvImage3.setImageResource(R.drawable.devide1);
            tvImage4.setImageResource(R.drawable.devide1);
            setDigit("Devide"+s1,"Devide"+s2,"Devide"+s3,
                    "Devide"+s4,"Ex:- 9 / 5 = 1","Ex:- 18 / 9 = 2","Ex:- 10 / 10 = 1","Ex:- 100 / 10 = 10");
        }
    }
void setDigit(String tv1, String tv2, String tv3, String tv4, String ex1, String ex2, String ex3, String ex4){
    tvText1.setText(tv1);
    tvText2.setText(tv2);
    tvText3.setText(tv3);
    tvText4.setText(tv4);
    tvExample1.setText(ex1);
    tvExample2.setText(ex2);
    tvExample3.setText(ex3);
    tvExample4.setText(ex4);
}
    void sendDigitByIntent(String length1,String length2,String length3,String length4,String s){
        String str = getIntent().getStringExtra("digit_key");
        Intent i = new Intent(this, Quiz.class);
        i.putExtra("operation_key",str);
        i.putExtra("length1",length1);
        i.putExtra("length2",length2);
        i.putExtra("length3",length3);
        i.putExtra("length4",length4);
        i.putExtra("digit",s);
        startActivity(i);
    }

    @OnClick(R.id.cv1)
    public void onCv1Clicked() {
        sendDigitByIntent("10","1","10","1",s1);
    }

    @OnClick(R.id.cv2)
    public void onCv2Clicked() {
        sendDigitByIntent("100","10","10","1",s2);
    }

    @OnClick(R.id.cv3)
    public void onCv3Clicked() {
        sendDigitByIntent("100","10","100","10",s3);
    }

    @OnClick(R.id.cv4)
    public void onCv4Clicked() {
        sendDigitByIntent("999","100","100","10",s4);
    }
}
