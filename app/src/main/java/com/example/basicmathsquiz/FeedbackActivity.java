package com.example.basicmathsquiz;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedbackActivity extends BaseActivity {
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_feedback)
    EditText etFeedback;
    @BindView(R.id.btnFeedback)
    Button btnFeedback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity);
        ButterKnife.bind(this);
        setupActionBar("Feedback", true);
    }

    @OnClick(R.id.btnFeedback)
    public void onViewClicked() {
        sendFeedback();
    }

    void sendFeedback(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/email");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"chauhanjyoti846@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT,"BasicMaths Quiz App");
        intent.putExtra(Intent.EXTRA_TEXT,"Name : "+etName.getText()+"\n Message : "+etFeedback.getText());
        try {
            startActivity(Intent.createChooser(intent,"Please select Email"));

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
