package com.example.basicmathsquiz;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.example.basicmathsquiz.database.Answer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.cvAddition)
    CardView cvAddition;
    @BindView(R.id.cvSubstraction)
    CardView cvSubstraction;
    @BindView(R.id.cvMultiplication)
    CardView cvMultiplication;
    @BindView(R.id.cvDivision)
    CardView cvDivision;
    public static final String ADDITION = "addition";
    public static final String SUBSTRACTION = "substraction";
    public static final String MULTIPLY = "multiply";
    public static final String DIVISION = "division";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupActionBar("BasicMaths Quiz", false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_option,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.dashbord :
                break;
            case R.id.share :
                try {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_SUBJECT,"BasicMathsQuiz");
                    String shareMessage = "https://play.google.com/store/apps/details?id="+BuildConfig.APPLICATION_ID+"\n\n";
                    intent.putExtra(Intent.EXTRA_TEXT,shareMessage);
                    startActivity(Intent.createChooser(intent,"share by"));
                }catch (Exception e){
                    Toast.makeText(this,"Error Occured",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rate :
                Intent i = new Intent(this,AnswerListActivity.class);
                startActivity(i);
                break;
            case R.id.feedback :
                Intent intent = new Intent(this,FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.developer :
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void sendDataByIntent(String msg){
        Intent i = new Intent(MainActivity.this, DigitSetActivity.class);
        i.putExtra("digit_key",msg);
        startActivity(i);
    }

    @OnClick(R.id.cvAddition)
    public void onCvAdditionClicked() {
        sendDataByIntent(ADDITION);
    }

    @OnClick(R.id.cvSubstraction)
    public void onCvSubstractionClicked() {
        sendDataByIntent(SUBSTRACTION);
    }

    @OnClick(R.id.cvMultiplication)
    public void onCvMultiplicationClicked() {
        sendDataByIntent(MULTIPLY);
    }

    @OnClick(R.id.cvDivision)
    public void onCvDivisionClicked() {
       sendDataByIntent(DIVISION);
    }
}