package com.example.basicmathsquiz;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.basicmathsquiz.database.Answer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Quiz extends BaseActivity {
    @BindView(R.id.tvQuestionNumber)
    TextView tvQuestionNumber;
    @BindView(R.id.wrong)
    TextView wrong;
    @BindView(R.id.tvright)
    TextView tvright;
    @BindView(R.id.tvQuestion)
    TextView tvQuestion;
    @BindView(R.id.tvOption1)
    TextView tvOption1;
    @BindView(R.id.tvOption2)
    TextView tvOption2;
    @BindView(R.id.tvOption3)
    TextView tvOption3;
    @BindView(R.id.tvOption4)
    TextView tvOption4;
    int n = 3, i3, right = 1, rong = 1;
    int questionNumber = 1;
    boolean isOption1, isOption2, isOption3, isOption4;
    MediaPlayer correctSong;
    MediaPlayer incorrectSong;
    boolean timerStarted = false;
    Timer timer;
    TimerTask timerTask;
    Double time = 0.0;
    @BindView(R.id.tvTimer)
    TextView tvTimer;
    @BindView(R.id.btn_finishQuiz)
    Button btnFinishQuiz;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ButterKnife.bind(this);
        setupActionBar("Quiz", true);
        startQuiz();
        setMusic();

    }

    @Override
    protected void onPause() {
        super.onPause();
        correctSong.stop();
        incorrectSong.stop();
    }

    void setMusic() {
        correctSong = MediaPlayer.create(this, R.raw.correctanswer);
        incorrectSong = MediaPlayer.create(this, R.raw.incorrectanswer);
        timer = new Timer();
        setTimer();
    }

    void startQuiz() {
        isOption1 = false;
        isOption2 = false;
        isOption3 = false;
        isOption4 = false;
        setBackgroundWhite();
        tvQuestionNumber.setText("Question Number : " + questionNumber++);
        String str = getIntent().getStringExtra("operation_key");
        int length1 = Integer.parseInt(getIntent().getStringExtra("length1"));
        int length2 = Integer.parseInt(getIntent().getStringExtra("length2"));
        int length3 = Integer.parseInt(getIntent().getStringExtra("length3"));
        int length4 = Integer.parseInt(getIntent().getStringExtra("length4"));
        if (str.equals("addition")) {
            if (length1 == 10 && length2 == 1 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("addition", 13, 1);
            } else if (length1 == 100 && length2 == 10 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("addition", 62, 45);
            } else if (length1 == 100 && length2 == 10 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("addition", 125, 70);
            } else if (length1 == 999 && length2 == 100 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("addition", 1000, 300);
            }
        } else if (str.equals("substraction")) {
            if (length1 == 10 && length2 == 1 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("substraction", 7, 1);
            } else if (length1 == 100 && length2 == 10 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("substraction", 90, 10);
            } else if (length1 == 100 && length2 == 10 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("substraction", 50, 10);
            } else if (length1 == 999 && length2 == 100 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("substraction", 500, 100);
            }
        } else if (str.equals("multiply")) {
            if (length1 == 10 && length2 == 1 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("multiplication", 70, 10);
            } else if (length1 == 100 && length2 == 10 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("multiplication", 200, 30);
            } else if (length1 == 100 && length2 == 10 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("multiplication", 4000, 1000);

            } else if (length1 == 999 && length2 == 100 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("multiplication", 30000, 5000);
            }
        } else {
            if (length1 == 10 && length2 == 1 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("division", 10, 1);
            } else if (length1 == 100 && length2 == 10 && length3 == 10 && length4 == 1) {
                setQuestionAnswer("division", 50, 15);
            } else if (length1 == 100 && length2 == 10 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("division", 20, 1);
            } else if (length1 == 999 && length2 == 100 && length3 == 100 && length4 == 10) {
                setQuestionAnswer("division", 20, 1);
            }
        }
    }

    int generateRandomNumber(int l1, int l2) {
        Random r = new Random();
        return r.nextInt(l1 - l2) + l2;
    }

    void setBackgroundWhite() {
        tvOption1.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption2.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption3.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption4.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
    }

    void setAnswerBackground(TextView tvTrueOption, boolean flag) {
        tvOption1.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption2.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption3.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        tvOption4.setBackground(getResources().getDrawable(R.drawable.option_backgroun));
        if (flag) {
            tvTrueOption.setBackground(getResources().getDrawable(R.drawable.set_green_backgroud));
            tvright.setText("" + right++);
            correctSong.start();
        } else {
            tvTrueOption.setBackground(getResources().getDrawable(R.drawable.set_red_background));
            wrong.setText("" + rong++);
            incorrectSong.start();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startQuiz();
                resetTimer();
            }
        }, 200);
    }

    @OnClick(R.id.tvOption1)
    public void onTvOption1Clicked() {
        setAnswerBackground(tvOption1, isOption1);
    }

    @OnClick(R.id.tvOption2)
    public void onTvOption2Clicked() {
        setAnswerBackground(tvOption2, isOption2);
    }

    @OnClick(R.id.tvOption3)
    public void onTvOption3Clicked() {
        setAnswerBackground(tvOption3, isOption3);
    }

    @OnClick(R.id.tvOption4)
    public void onTvOption4Clicked() {
        setAnswerBackground(tvOption4, isOption4);
    }

    void setQuestionAnswer(String operation, int l1, int l2) {
        int length1 = Integer.parseInt(getIntent().getStringExtra("length1"));
        int length2 = Integer.parseInt(getIntent().getStringExtra("length2"));
        int length3 = Integer.parseInt(getIntent().getStringExtra("length3"));
        int length4 = Integer.parseInt(getIntent().getStringExtra("length4"));
        int i1 = generateRandomNumber(length1, length2);
        int i2 = generateRandomNumber(length3, length4);
        ArrayList<Integer> list = new ArrayList<>();
        if (operation.equals("addition")) {
            tvQuestion.setText("" + i1 + "   +   " + i2 + "   =  ? ");
            i3 = i1 + i2;
        } else if (operation.equals("substraction")) {
            tvQuestion.setText("" + i1 + "   -   " + i2 + "   =  ? ");
            i3 = i1 - i2;
        } else if (operation.equals("multiplication")) {
            tvQuestion.setText("" + i1 + "   *   " + i2 + "   =  ? ");
            i3 = i1 * i2;
        } else if (operation.equals("division")) {
            tvQuestion.setText("" + i1 + "   /   " + i2 + "   =  ? ");
            i3 = i1 / i2;
        }
        list.add(i3);
        list.add(generateRandomNumber(l1, l2));
        list.add(generateRandomNumber(l1, l2));
        list.add(generateRandomNumber(l1, l2));

        Collections.shuffle(list);

        tvOption1.setText("" + list.get(0));
        tvOption2.setText("" + list.get(1));
        tvOption3.setText("" + list.get(2));
        tvOption4.setText("" + list.get(3));
        if (list.get(0) == i3) {
            isOption1 = true;
        }
        if (list.get(1) == i3) {
            isOption2 = true;
        }
        if (list.get(2) == i3) {
            isOption3 = true;
        }
        if (list.get(3) == i3) {
            isOption4 = true;

        }
    }

    void setTimer() {
        if (timerStarted == false) {
            timerStarted = true;
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            time++;
                            tvTimer.setText(getTimerText());
                            if (time >= 10) {
                                timerTask.cancel();
                                startQuiz();
                                resetTimer();
                            }
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(timerTask, 1000, 1000);
        }
    }

    public String getTimerText() {
        int rounded = (int) Math.round(time);
        int secound = ((rounded % 86400) % 3600) % 60;
        int minute = ((rounded % 86400) % 3600) / 60;
        return formatTime(secound, minute);
    }

    public String formatTime(int seconds, int minute) {
        return String.format("%02d", minute) + " : " + String.format("%02d", seconds);
    }

    void resetTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            time = 0.0;
            timerStarted = false;
            tvTimer.setText(formatTime(0, 0));
            setTimer();
        }
    }

    @OnClick(R.id.btn_finishQuiz)
    public void onViewClicked() {
        String str = getIntent().getStringExtra("operation_key");
        String digit = getIntent().getStringExtra("digit");
        final Calendar newCalendar = Calendar.getInstance();
        String date =  String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR));
        long lastInsertedId = new Answer(getApplicationContext()).insertAnswer(str,digit,questionNumber,right,rong,date);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Quiz.this,AnswerListActivity.class);
                startActivity(intent);
            }
        }, 200);
    }
}
