package com.example.basicmathsquiz.model;

import java.io.Serializable;

public class AnswerModel implements Serializable {
    int id;
    String operation;
    String digit;
    int attemp_question;
    int right_ansewr;
    int rong_answer;
    String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }

    public int getAttemp_question() {
        return attemp_question;
    }

    public void setAttemp_question(int attemp_question) {
        this.attemp_question = attemp_question;
    }

    public int getRight_ansewr() {
        return right_ansewr;
    }

    public void setRight_ansewr(int right_ansewr) {
        this.right_ansewr = right_ansewr;
    }

    public int getRong_answer() {
        return rong_answer;
    }

    public void setRong_answer(int rong_answer) {
        this.rong_answer = rong_answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "AnswerModel{" +
                "id=" + id +
                ", operation='" + operation + '\'' +
                ", digit='" + digit + '\'' +
                ", attemp_question=" + attemp_question +
                ", right_ansewr=" + right_ansewr +
                ", rong_answer=" + rong_answer +
                ", date=" + date +
                '}';
    }
}
